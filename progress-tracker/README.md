# progress-tracker

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Install serve
```
npm install -g serve
```

### Run on production environment
```
serve -s dist
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
